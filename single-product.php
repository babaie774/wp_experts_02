<?php get_header(); ?>
<?php get_template_part( 'partials/top-bar' ); ?>
<?php get_template_part( 'partials/top-header' ); ?>
<?php get_template_part( 'partials/top-menu-store' ); ?>

	<div id="wrapper">
		<div id="content-wrap">
			<?php if(have_posts()): ?>
				<?php while (have_posts()):the_post(); ?>
					<div class="post">
						<h4 class="post-title">
							<?php the_title(); ?>
						</h4>
						<div class="post-content">
							<div class="post-description">
								<?php the_content() ?>
							</div>
							<div class="post-details">
								<?php if(has_post_thumbnail()): ?>
										<?php echo get_the_post_thumbnail(); ?>
								<?php endif; ?>
							</div>
							<div class="clearfix"></div>
							<div class="add_to_basket">
								<a data-pid="<?php echo get_the_ID();  ?>" class="add_to_basket_btn" href="<?php  ?>">اضافه به سبد خرید</a>
							</div>
						</div>
						<div class="post-details">
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
<?php get_footer() ?>