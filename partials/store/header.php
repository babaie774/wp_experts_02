<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="<?php echo WPEXP_CSS_URL.'bootstrap-rtl.min.css'; ?>">
	<link rel="stylesheet" href="<?php echo WPEXP_URL.'/fonts.css'; ?>">
	<link rel="stylesheet" href="<?php echo WPEXP_CSS_URL.'store.css'; ?>">
	<?php wp_head(); ?>
</head>
<body>
