<div id="top-menu-store">
    <?php
        if(has_nav_menu('wpx-top-menu')):
//	        $nav_menu_content = \Application\Utility\WPX_Cache::get('main_menu');
//            ob_start();
	        wp_nav_menu([ 'theme_location' => 'wpx-top-menu' ]);
//	        $nav_menu_content = ob_get_clean();
//	        \Application\Utility\WPX_Cache::set('main_menu',$nav_menu_content);
        else:
            echo '<span>برای این قسمت یک منو ایجاد کنید</span>';
        endif;
    ?>
	<div class="basket-wrapper">
		<a class="basketCounterWrapper animated"  href="/store/basket">
			<span>سبد خرید</span>
			<span class="basketCounter"><?php echo \Application\Service\Basket\Basket::getItemsCount(); ?></span>
		</a>
	</div>
</div>