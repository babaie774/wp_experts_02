<div id="store-slider">
	<?php

	$slider_posts = new WP_Query( [
		'post_type' => 'product',
		'meta_key'  => 'wpx_slider'
	] );

	?>
	<?php if ( $slider_posts->have_posts() ): ?>
        <ul class="rslides" id="slider2">
			<?php while ( $slider_posts->have_posts() ):$slider_posts->the_post(); ?>
                <li><a href="<?php echo get_the_permalink($slider_posts->post->ID); ?>"><img src="<?php echo get_post_meta($slider_posts->post->ID,'wpx_slider_image',true);  ?>" alt="<?php echo get_the_title($slider_posts->post->ID); ?>"></a></li>
			<?php endwhile; ?>
        </ul>
	<?php endif; ?>
</div>