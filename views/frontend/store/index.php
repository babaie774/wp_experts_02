<?php get_header(); ?>
<?php get_template_part( 'partials/top-bar' ); ?>
<?php get_template_part( 'partials/top-header' ); ?>
<?php get_template_part( 'partials/top-menu-store' ); ?>
    <?php get_template_part( 'partials/components/slider' ); ?>
    <div id="store-wrapper">
		<?php if ( $products->have_posts() ): ?>
			<?php while ( $products->have_posts() ):$products->the_post(); ?>
            <?php $current_post = $products->post; ?>
            <div class="product-item">
                <div class="product-image">
                    <?php echo  get_the_post_thumbnail($current_post->ID); ?>
                </div>
                <h6>
                    <a href="<?php echo get_the_permalink(); ?>"><?php echo  wp_trim_words(get_the_title($current_post->ID),10); ?></a>
                </h6>
                <div class="product-price">
                    <?php
                    $price = get_post_meta($current_post->ID,'product_price',true);
                    echo \Application\Utility\PersianNumbers::convert(number_format($price));
                    ?>
                </div>
                <div class="add_to_basket">
                    <a data-pid="<?php echo $current_post->ID;  ?>" class="add_to_basket_btn" href="<?php  ?>">اضافه به سبد خرید</a>
                </div>
                <div class="add_to_whislist">
                    <a href="<?php echo add_query_arg(['add_to_wishlist' => $current_post->ID]); ?>">اضافه به علاقه مندی ها</a>
                </div>
            </div>
			<?php endwhile; ?>
		<?php endif; ?>
    </div>
<?php get_footer(); ?>