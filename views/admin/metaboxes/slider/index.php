<div class="wrap">
	<input type="checkbox" id="wpx_product_slider" name="wpx_product_slider" <?php checked(1,intval($is_slider)); ?>>
	<label for="wpx_product_slider">انتخاب این محصول برای نمایش در اسلایدر</label>
	<div class="wpx_slider_banner_wrapper" style="margin: 20px 0;<?php echo intval($is_slider) == 0 ? 'display: none;' : ''; ?>">
        <img style="width: 150px;height: 150px;" src="<?php echo !empty($slider_image) ? $slider_image : ''; ?>" alt="">
        <button class="button wpx_select_slider_btn" name="wpx_select_slider_btn">انتخاب بنر</button>
        <input type="hidden" class="wpx_product_slider_image" name="wpx_product_slider_image">
    </div>
</div>