<?php


namespace Application\Repositories;


class OrderItemRepository extends BaseRepository {

	public function __construct() {
		parent::__construct();
		$this->table = $this->prefix.'order_items';
	}

}