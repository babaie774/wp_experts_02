<?php


namespace Application\Repositories;


use Application\Models\OrderStatus;

class OrderRepository extends BaseRepository {

	public function __construct() {
		parent::__construct();
		$this->table = $this->prefix . 'orders';
		$this->primary_key = 'order_id';
	}

	public function updateOrderStatus( int $order_id, int $newStatus ) {
		return $this->db->update( $this->table,
			[
				'order_status'  => OrderStatus::PAID,
				'order_paid_at' => date( 'Y-m-d H:i:s' )
			],
			[ 'order_id' => $order_id ],
			[ '%d', '%s' ],
			[ '%d' ]
		);
	}

}