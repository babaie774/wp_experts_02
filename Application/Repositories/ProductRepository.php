<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/18/2017
 * Time: 5:43 PM
 */

namespace Application\Repositories;


class ProductRepository extends BaseRepository {

	public function get_latest_products() {
		$all_product_query = new \WP_Query( [
			'post_type' => 'product'
		] );
		return $all_product_query;
//		return $this->db->get_results("");
	}

	public function find( $id ) {
		return get_post($id);
	}

}