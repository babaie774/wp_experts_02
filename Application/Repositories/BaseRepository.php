<?php

namespace Application\Repositories;


class BaseRepository {

	protected $db;

	protected $table;

	protected $prefix;

	protected $primary_key;

	public function __construct() {
		global $wpdb;
		$this->db     = $wpdb;
		$this->prefix = $wpdb->prefix;
	}

	public function find( $id ) {
		return $this->db->get_row( "SELECT * FROM {$this->table} WHERE {$this->primary_key}={$id} LIMIT 1" );
	}

	public function findBy( array $criteria, $is_single = false ) {
		$method = 'get_results';
		if ( $is_single ) {
			$method = 'get_row';
		}
		$where_query = "1";
		foreach ( $criteria as $key => $value ) {
			$where_query .= " AND {$key}={$value} ";
		}

		return $this->db->$method( "SELECT * FROM {$this->table} WHERE {$where_query}" );

	}

	public function all() {
		return $this->db->get_results( "SELECT * FROM {$this->table}" );
	}

	public function paginate() {

	}

	public function delete( $id ) {

	}

	public function update( $id, $data ) {
		$dataFormat = $this->generateDataFormat( $data );
		 $this->db->update( $this->table, $data, [ $this->primary_key => $id ], $dataFormat, [ '%d' ] );
	}

	public function updateBy( array $criteria, array $data ) {
		$whereFormat = $this->generateDataFormat( $criteria );
		$dataFormat  = $this->generateDataFormat( $data );

		return $this->db->update( $this->table, $data, $criteria, $dataFormat, $whereFormat );

	}

	public function create( array $data ) {//array $format
		$format = $this->generateDataFormat( $data );
		$this->db->insert( $this->table, $data, $format );

		return $this->db->insert_id;
	}

	private function generateDataFormat( array $data ) {
		$result = [];
		foreach ( $data as $item ) {
			switch ( true ) {
				case is_int( $item ):
					$result[] = '%d';
					break;
				case is_string( $item ):
					$result[] = '%s';
					break;
				case is_float( $item ) && is_double( $item ):
					$result[] = '%f';
					break;
				default:
					$result[] = '%s';
					break;
			}
		}

	}

}