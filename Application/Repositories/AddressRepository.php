<?php


namespace Application\Repositories;


class AddressRepository extends BaseRepository {

	public function __construct() {
		parent::__construct();
		$this->table = $this->prefix.'addresses';
	}

}