<?php


namespace Application\Repositories;


class PostsRepository extends BaseRepository {

	public function __construct() {
		parent::__construct();
		$this->table = $this->db->posts;
	}

	public function getAllPosts($criteria) {
		$api_post_args = array_merge([
			'post_type' => 'product'
		],$criteria);
		$api_posts = new \WP_Query($api_post_args);
		return $api_posts;
	}

}