<?php


namespace Application\Repositories;


class PaymentRepository extends BaseRepository {

	public function __construct() {
		parent::__construct();
		$this->table = $this->prefix . 'payments';
		$this->primary_key = 'payment_id';
	}

}