<?php

namespace Application\Admin\Metaboxes;

use Application\Admin\Metaboxes\Contract\BaseMetaBox;
use Application\Utility\View;

class PriceMetaBox extends BaseMetaBox {
	public function __construct() {
		$this->id      = 'wpx_price_meta_box';
		$this->title   = 'قیمت محصول';
		$this->screens = [ 'product' ];
		parent::__construct();
	}

	public function render( \WP_Post $post ) {
		$params = [
			'product_price' => get_post_meta($post->ID,'product_price',true)
		];
		View::load( 'admin.metaboxes.price.index', compact( 'params' ) );
	}

	public function save( int $post_id ) {
		if(defined('DOING_AJAX') && DOING_AJAX)
		{
			return;
		}
		if(!current_user_can('edit_posts'))
		{
			return;
		}
		if ( isset( $_POST['wpx_product_price'] ) && intval( $_POST['wpx_product_price'] ) > 0 ) {
			update_post_meta($post_id,'product_price',intval($_POST['wpx_product_price']));
		}
	}
}