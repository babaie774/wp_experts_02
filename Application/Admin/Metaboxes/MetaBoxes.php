<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/18/2017
 * Time: 3:46 PM
 */

namespace Application\Admin\Metaboxes;


class MetaBoxes {

	public static function load() {
		new PriceMetaBox();
		new SliderMetaBox();
	}

}