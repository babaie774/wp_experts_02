<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/20/2017
 * Time: 4:15 PM
 */

namespace Application\Admin\Metaboxes;


use Application\Admin\Metaboxes\Contract\BaseMetaBox;
use Application\Utility\View;

class SliderMetaBox extends BaseMetaBox {
	public function __construct() {
		$this->id      = 'slider-meta-box';
		$this->title   = 'نمایش این محصول در اسلایدر';
		$this->screens = [ 'product' ];
		parent::__construct();
	}

	public function render( \WP_Post $post ) {
		$is_slider = get_post_meta($post->ID,'wpx_slider',true);
		$slider_image = get_post_meta($post->ID,'wpx_slider_image',true);
		View::load( 'admin.metaboxes.slider.index',compact('is_slider','slider_image') );
	}

	public function save( int $post_id ) {
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
			return false;
		}
		if ( ! current_user_can( 'edit_posts' ) ) {
			return false;
		}
		if ( isset( $_POST['wpx_product_slider'] ) ) {
			update_post_meta( $post_id, 'wpx_slider', 1 );
			update_post_meta($post_id,'wpx_slider_image',$_POST['wpx_product_slider_image']);
		} else {
			delete_post_meta( $post_id, 'wpx_slider' );
			delete_post_meta($post_id,'wpx_slider_image');
		}
	}
}