<?php


namespace Application\Controllers\Api;


use Application\Repositories\PostsRepository;
use Application\Utility\PersianNumbers;

class PostsController {

	public static function index() {

		$post_repository = new PostsRepository();
		$criteria = [];
		if(isset($_GET['search']) && !empty($_GET['search']))
		{
			$criteria['s']= $_GET['search'];
		}
		$api_posts       = $post_repository->getAllPosts($criteria);
		$results         = [];
		if ( $api_posts->have_posts() ):
			while ( $api_posts->have_posts() ):$api_posts->the_post();
				$results[] = [
					'title' => get_the_title( $api_posts->post->ID ),
					'product_price' => PersianNumbers::convert(get_post_meta($api_posts->post->ID,'product_price',true) )
				];
			endwhile;
		endif;

		return wp_send_json( $results );

	}

	public static function create() {
		$my_post = array(
			'post_title'    => wp_strip_all_tags( $_POST['post_title'] ),
			'post_content'  => $_POST['post_content'],
			'post_status'   => 'publish',
			'post_author'   => 1,
		);
		$new_post_id = wp_insert_post($my_post);
		if(intval($new_post_id) > 0)
		{
			wp_send_json([
				'success' => true,
				'message' => 'مطلب جدید با موفقیت ایجاد شد'
			]);
		}
		wp_send_json([
			'success' => false,
			'message' => 'خطایی در ثبت مطلب جدید اتفاق افتاده است'
		]);
	}

}