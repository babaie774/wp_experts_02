<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/18/2017
 * Time: 5:14 PM
 */

namespace Application\Controllers\Store;


use Application\Auth\Auth;
use Application\Models\OrderStatus;
use Application\Repositories\AddressRepository;
use Application\Repositories\OrderItemRepository;
use Application\Repositories\OrderRepository;
use Application\Repositories\ProductRepository;
use Application\Service\Basket\Basket;
use Application\Utility\View;

class StoreController {

	public static function index() {
		$product_repository = new ProductRepository();
		$products           = $product_repository->get_latest_products();

		View::load( 'frontend.store.index', compact( 'products' ) );
	}

	public function basket() {

		$basket_items = Basket::getItems();
		View::load( 'frontend.store.basket', compact( 'basket_items' ) );
	}

	public function checkout() {
		$order_repository      = new OrderRepository();
		$order_item_repository = new OrderItemRepository();
		$address_repository    = new AddressRepository();
		$basket_items          = Basket::getItems();
		$new_order_id          = $order_repository->create( [
			'order_user_id'     => Auth::current_id(),
			'order_total_price' => Basket::totalPrice(),
			'order_created_at'  => date( 'Y-m-d H:i:s' ),
			'order_status'      => OrderStatus::UNPAID
		] );
		if ( $new_order_id ) {
			foreach ( $basket_items as $product_id => $order_item ) {
				$new_order_item = $order_item_repository->create( [
					'order_item_order_id'   => $new_order_id,
					'order_item_product_id' => $product_id,
					'order_item_price'      => $order_item['price'],
					'order_item_count'      => $order_item['count']
				] );
			}
		}
		$user_addresses = $address_repository->findBy( [ 'address_user_id' => Auth::current_id() ] );
		View::load( 'frontend.store.checkout',compact('user_addresses','new_order_id'));
	}

}