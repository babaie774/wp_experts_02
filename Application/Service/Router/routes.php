<?php
return [
	'/store'   => '\Application\Controllers\Store\StoreController::index',
	'/store/basket'   => '\Application\Controllers\Store\StoreController::basket',
	'/store/checkout'   => '\Application\Controllers\Store\StoreController::checkout',
	'/store/payment'   => '\Application\Controllers\Store\PaymentsController::payment',
	'/api/products'   => '\Application\Controllers\Api\PostsController::index',
	'/api/posts/create'   => '\Application\Controllers\Api\PostsController::create',
];