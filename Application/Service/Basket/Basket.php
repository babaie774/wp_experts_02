<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/18/2017
 * Time: 6:46 PM
 */

namespace Application\Service\Basket;


use Application\Repositories\ProductRepository;

class Basket {
	public function __construct() {
		$this->initBasket();
	}

	public function add( $product_id ) {
		$product_repository = new ProductRepository();
		$product            = $product_repository->find( $product_id );
		if ( $product ) {
			if ( isset( $_SESSION['basket']['items'][ $product_id ] ) ) {
				$_SESSION['basket']['items'][ $product_id ]['count'] ++;
			} else {
				$_SESSION['basket']['items'][ $product_id ] = [
					'price' => get_post_meta( $product->ID, 'product_price', true ),
					'title' => $product->post_title,
					'count' => 1
				];
			}
			$this->calculateTotalPrice();

			return true;
		}

		return false;
	}

	public function remove( $product_id ) {
		if ( isset( $_SESSION['basket']['items'][ $product_id ] ) ) {
			unset( $_SESSION['basket']['items'][ $product_id ] );
			$this->calculateTotalPrice();
		}
	}

	public function updateCount( $product_id, $count ) {
		if ( isset( $_SESSION['basket']['items'][ $product_id ] ) ) {
			$_SESSION['basket']['items'][ $product_id ]['count'] = $count;
			$this->calculateTotalPrice();
		}
	}

	public function subTotal( $product_id ) {
		if ( isset( $_SESSION['basket']['items'][ $product_id ] ) ) {
			$item = $_SESSION['basket']['items'][ $product_id ];

			return (int) $item['price'] * (int) $item['count'];
		}

		return 0;
	}

	public static function getItems() {
		if ( isset( $_SESSION['basket']['items'] ) ) {
			return $_SESSION['basket']['items'];
		}

		return [];
	}

	public static function getItemsCount() {
		if ( isset( $_SESSION['basket']['items'] ) ) {
			return count($_SESSION['basket']['items']);
		}

		return 0;
	}

	private function initBasket() {
		if ( ! isset( $_SESSION['basket']['items'] ) ) {
			$_SESSION['basket']['items'] = [];
		}
	}

	public static function totalPrice() {
		return isset( $_SESSION['basket']['totalPrice'] ) ? (int) $_SESSION['basket']['totalPrice'] : 0;
	}

	private function calculateTotalPrice() {
		if ( isset( $_SESSION['basket']['items'] ) ) {
			$_SESSION['basket']['totalPrice'] = 0;
			foreach ( $_SESSION['basket']['items'] as $item ) {
				$_SESSION['basket']['totalPrice'] += (int) $item['price'] * (int) $item['count'];
			}
		}
	}
}
