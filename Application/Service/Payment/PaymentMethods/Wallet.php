<?php


namespace Application\Service\Payment\PaymentMethods;


use Application\Service\Payment\Contract\PaymentMethod;
use Application\Service\Payment\PaymentMethods;
use Application\Service\Payment\PaymentStatus;
use Application\Utility\Payment;
use Application\Utility\User;

class Wallet extends PaymentMethod {
	public static $title = 'حساب کاربری';
	public static $id = 'wallet';

	public function __construct( int $order_id ) {
		parent::__construct( $order_id );
	}

	public function pay() {

		$user           = new \WP_User( $this->order_item->order_user_id );
		$user_wallet    = User::getWallet( $this->order_item->order_user_id );
		$new_payment_id = $this->payment_repository->create( [
			'payment_order_id'   => $this->order_id,
			'payment_type'       => PaymentMethods::WALLET,
			'payment_amount'     => $this->order_item->order_total_price,
			'payment_res_num'    => Payment::generateResNum() . $this->order_item->order_user_id,
			'payment_created_at' => date( 'Y-m-d H:i:s' ),
			'payment_status'     => PaymentStatus::FAILED
		] );
		if ( intval( $user_wallet ) < intval( $this->order_item->order_total_price ) ) {
			$this->payment_repository->update( $new_payment_id, [ 'payment_description' => 'مقدار موجودی حساب کاربری کافی نمی باشد.' ] );

			return [
				'status'  => false,
				'message' => 'مقدار موجودی حساب کاربری کافی نمی باشد.'
			];
		}

		$paymentResult = User::payFromWallet( $this->order_item->order_user_id, $this->order_item->order_total_price );
		if ( $paymentResult ) {
			$this->payment_repository->update( $new_payment_id, [ 'payment_status' => PaymentStatus::SUCCESS ] );
			return [
				'status'     => true,
				'payment_id' => $new_payment_id,
				'message'  => 'پرداخت با موفقیت ثبت شد'
			];
		}
	}
}