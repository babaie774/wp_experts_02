<?php


namespace Application\Service\Payment;


use Application\Service\Payment\PaymentMethods\Cod;
use Application\Service\Payment\PaymentMethods\Gateway;
use Application\Service\Payment\PaymentMethods\Wallet;

class PaymentMethods {
	const WALLET = 1;
	const GATEWAY = 2;
	const COD = 3;
	public static function gateways() {
		return apply_filters( 'wpx_payment_methods', [
			Wallet::class,
			Cod::class,
			Gateway::class
		] );
	}

	public static function getPaymentClassById( string $id ) {
		return [
			'wallet'  => Wallet::class,
			'cod'     => Cod::class,
			'gateway' => Gateway::class
		][ $id ];
	}

}