<?php


namespace Application\Service\Payment;


class PaymentStatus {

	const SUCCESS = 1;
	const FAILED = 0;

	public static function getPaymentStatuses() {
		return [
			self::SUCCESS   => 'موفق',
			self::FAILED => 'ناموفق '
		];
	}

}