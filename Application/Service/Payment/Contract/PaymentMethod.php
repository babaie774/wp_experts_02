<?php


namespace Application\Service\Payment\Contract;


use Application\Repositories\OrderRepository;
use Application\Repositories\PaymentRepository;

abstract class PaymentMethod {
	protected $order_item;
	protected $order_id;
	protected $order_repository;
	protected $payment_repository;
	public function __construct(int $order_id) {
		$this->order_repository = new OrderRepository();
		$this->order_id = $order_id;
		$this->order_item = $this->order_repository->find($order_id);
		$this->payment_repository = new PaymentRepository();
	}

	abstract public function pay();

}