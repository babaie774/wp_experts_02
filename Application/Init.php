<?php

namespace Application;

use Application\Auth\Auth;
use Application\Repositories\WishListRepository;

class Init {

	public static function theme_setup() {
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'show-basket' );
		load_theme_textdomain('wpx',get_template_directory().'/lang');
		register_sidebar( array(
			'name'          => __( 'بخش کناری سایت', 'wpexp' ),
			'id'            => 'main-sidebar',    // ID should be LOWERCASE  ! ! !
			'description'   => 'Main Sidebar',
			'class'         => 'wpexp-main-sidebar',
			'before_widget' => '<div class="widget main-sidebar">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>'
		) );
		register_nav_menu( 'wpx-top-menu', 'WPX Theme Main Menu' );

	}

	public static function start_session() {
		session_start();
	}

	public static function check_wishlist() {
		$product_id = isset( $_GET['add_to_wishlist'] ) && intval( $_GET['add_to_wishlist'] ) > 0 ? intval( $_GET['add_to_wishlist'] ) : null;
		if ( $product_id ) {
			if ( Auth::check() ) {
				$wishlist_repository = new WishListRepository();
				$wishlist_repository->create( [
					'wishlist_user_id'    => Auth::current_id(),
					'wishlist_product_id' => $product_id
				] );
			}
		}
	}

}