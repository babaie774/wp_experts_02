<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/11/2017
 * Time: 5:56 PM
 */

namespace Application\Auth;


class Auth {

	public static function check() {
		return is_user_logged_in();
	}

	public static function current() {
		if ( self::check() ) {
			return wp_get_current_user();
		}

		return null;

	}

	/**
	 * get current user id
	 * @return int|null
	 */
	public static function current_id() {
		$current_user = self::current();

		return ! is_null( $current_user ) && $current_user instanceof \WP_User ? $current_user->ID : null;
	}

	public static function attempt( $email, $password, $remember = false ) {

		$result = wp_authenticate_email_password( null, $email, $password );
		if ( is_wp_error( $result ) ) {
			return false;
		}
		if ( $result instanceof \WP_User ) {
			return self::login( $email, $password, $remember );

		}

		return false;

	}

	public static function login( $email, $password, $remember = false ) {
		$user_item = get_user_by( 'email', $email );
		$user      = wp_signon( [
			'user_login'    => $user_item->user_login,
			'user_password' => $password,
			'remember'      => $remember
		], is_ssl() );
		if ( is_wp_error( $user ) ) {
			return false;//$user->get_error_message();
		}

		return true;
	}

}