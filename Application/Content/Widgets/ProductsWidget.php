<?php

namespace Application\Content\Widgets;


class ProductsWidget extends \WP_Widget {
	public function __construct() {
		parent::__construct(
			'wpx_products_widget',
			'ابزارک محصولات'
		);
		add_action( 'widgets_init', function () {
			register_widget( ProductsWidget::class );
		} );
	}

	public function widget( $args, $instance ) {

		echo $args['before_widget'];
		echo $args['before_title'] . 'محصولات' . $args['after_title'];
		$products_category_id = isset( $instance['products_category_id'] ) ? intval( $instance['products_category_id'] ) : 0;
		$products             = $this->getProductsByCategory( $products_category_id );
		if ( $products->have_posts() ):
			echo '<ul>';
			while ( $products->have_posts() ):$products->the_post();
				echo '<li><a href="' . get_the_permalink( $products->post->ID ) . '">' . get_the_title( $products->post->ID ) . '</a></li>';
			endwhile;
			echo '</ul>';
		else:
			echo '<div class="sidebar_no_posts"><p>محصولی یافت نشد</p></div>';
		endif;
		echo $args['after_widget'];
	}

	public function form( $instance ) {
		$post_categories          = get_terms( [
			'taxonomy'   => 'product',
			'hide_empty' => false
		] );
		$current_product_category = isset( $instance['products_category_id'] ) ? intval( $instance['products_category_id'] ) : 0;
		?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'products_category_id' ) ); ?>"><?php esc_attr_e( 'دسته بندی محصولات :', 'text_domain' ); ?></label>
            <select
                    class="widefat"
                    name="<?php echo esc_attr( $this->get_field_name( 'products_category_id' ) ); ?>"
                    id="<?php echo esc_attr( $this->get_field_id( 'products_category_id' ) ); ?>">
				<?php if ( $post_categories && count( $post_categories ) > 0 ): ?>
					<?php foreach ( $post_categories as $post_category ): ?>
                        <option
                                value="<?php echo $post_category->term_id ?>"
							<?php selected( $current_product_category, $post_category->term_id ); ?>
                        ><?php echo $post_category->name; ?></option>
					<?php endforeach; ?>
				<?php endif; ?>
            </select>
        </p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance                         = [];
		$instance['products_category_id'] = isset( $new_instance['products_category_id'] ) ? intval( $new_instance['products_category_id'] ) : 0;

		return $instance;
	}

	private function getProductsByCategory( $id ) {
		$products = new \WP_Query( [
			'post_type' => 'product',
			'tax_query' => [
				[
					'taxonomy' => 'product',
					'field'    => 'term_id',
					'terms'    => [ $id ]
				]
			]
		] );
		wp_reset_postdata();

		return $products;
	}
}
