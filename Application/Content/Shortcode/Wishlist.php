<?php


namespace Application\Content\Shortcode;


use Application\Auth\Auth;
use Application\Repositories\WishListRepository;
use Application\Utility\View;

class Wishlist {

	public static function callback( $atts, $content = null ) {

		if ( Auth::check() ) {
			$wishlist_repository = new WishListRepository();
			$all_user_wishlist   = $wishlist_repository->getUserWishList( Auth::current_id() );
			$result = '<p>در حال حاضر هیچ محصولی در لیست علاقه مندی شما وجود ندارد.</p>';
			if ( $all_user_wishlist  && count($all_user_wishlist) > 0) {
				$result = '<ul>';
				foreach ( $all_user_wishlist as $wishlist ) {
					$result .= '<li><a href="' . get_permalink( $wishlist->product_id ) . '">' . $wishlist->product_title . '</a></li>';
				}
				$result .= '</ul>';
			}
			return $result;
		}
		return '<p>در حال حاضر هیچ محصولی در لیست علاقه مندی شما وجود ندارد.</p>';

	}

}