<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/20/2017
 * Time: 6:08 PM
 */

namespace Application\Content\Taxonomies;


use Application\Content\Taxonomies\Contract\BaseTaxonomy;

class ProductTaxonomy extends BaseTaxonomy {
	public function __construct() {
		$this->id = 'product';
		$this->labels = [
			'name'                       => _x( 'دسته بندی محصولات', 'taxonomy general name', 'textdomain' ),
			'singular_name'              => _x( 'دسته بندی محصول', 'taxonomy singular name', 'textdomain' ),
			'search_items'               => __( 'جستجوی دسته بندی محصول', 'textdomain' ),
			'popular_items'              => __( 'محبوب ترین دسته بندی محصولات', 'textdomain' ),
			'all_items'                  => __( 'تمام دسته بندی ها', 'textdomain' ),
			'parent_item'                => null,
			'parent_item_colon'          => null,
			'edit_item'                  => __( 'Edit Writer', 'textdomain' ),
			'update_item'                => __( 'Update Writer', 'textdomain' ),
			'add_new_item'               => __( 'Add New Writer', 'textdomain' ),
			'new_item_name'              => __( 'New Writer Name', 'textdomain' ),
			'separate_items_with_commas' => __( 'Separate writers with commas', 'textdomain' ),
			'add_or_remove_items'        => __( 'Add or remove writers', 'textdomain' ),
			'choose_from_most_used'      => __( 'Choose from the most used writers', 'textdomain' ),
			'not_found'                  => __( 'No writers found.', 'textdomain' ),
			'menu_name'                  => __( 'دسته بندی محصولات', 'textdomain' ),
		];
		$this->types = ['product'];
		parent::__construct();
	}
}