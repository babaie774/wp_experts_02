<?php

namespace Application;


class Asset {
	public static function load() {
		self::register_scripts();
		self::register_styles();
	}

	public static function load_admin() {
		self::register_admin_scripts();
	}

	private static function register_styles() {
		wp_register_style( 'wpx_custom_front', WPEXP_CSS_URL . 'custom-frontend.css' );
		//wp_deregister_style('wpx_custom_front');
		wp_register_style( 'responsive_slider_css', WPEXP_LIBRARY_URL . 'responsiveslider/responsiveslides.css' );
		wp_register_style( 'swal', 'https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css' );
		wp_register_style( 'animate_css', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css' );
		wp_enqueue_style( 'swal' );
		wp_enqueue_style( 'animate_css' );
		wp_enqueue_style( 'wpx_custom_front' );
		wp_enqueue_style( 'responsive_slider_css' );
	}

	private static function register_scripts() {
//		if(is_single()){ //https://codex.wordpress.org/Conditional_Tags
//
//		}
		wp_register_script( 'swal_js', 'https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js' );
		wp_register_script( 'responsive_slider_js', WPEXP_LIBRARY_URL . 'responsiveslider/responsiveslides.min.js', [ 'jquery' ] );
		wp_register_script( 'wpx_custom_code', WPEXP_JS_URL . 'wpx_code.js', [
			'jquery',
			'swal_js',
			'responsive_slider_js'
		] );
		$data = [
			'taxRate' => .4,
			'ajaxUrl' => admin_url( 'admin-ajax.php' ),
			'min'     => 1000,
			'max'     => 5000
		];
//		wp_deregister_script('jquery');
		wp_localize_script( 'wpx_custom_code', 'wpx', $data );
		wp_enqueue_script( 'wpx_custom_code' );
	}

	private static function register_admin_scripts() {
		wp_register_script( 'wpx_product', WPEXP_JS_URL . 'admin/product.js', [ 'jquery' ] );
		wp_register_script( 'wpx_uploader', WPEXP_JS_URL . 'admin/uploader.js', [ 'jquery' ] );
		wp_enqueue_script( 'wpx_product' );
		wp_enqueue_media();
		wp_enqueue_script( 'wpx_uploader' );

	}

}