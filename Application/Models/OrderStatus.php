<?php


namespace Application\Models;


class OrderStatus {

	const UNPAID = 0;
	const PAID = 1;

	public static function getOrderStatuses() {
		return [
			self::PAID   => 'پرداخت شده',
			self::UNPAID => 'پرداخت نشده'
		];
	}

}