<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/18/2017
 * Time: 6:10 PM
 */

namespace Application\Utility;


class PersianNumbers {
	public static function convert($data) {
		$fa_numbers = ['۰','١','۲','۳','۴','۵','۶','۷','۸','۹'];
		$en_numbers = ['0','1','2','3','4','5','6','7','8','9'];
		return str_replace($en_numbers,$fa_numbers,$data);
	}
}