<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/20/2017
 * Time: 6:48 PM
 */

namespace Application\Utility;


class WPX_Cache {

	public static function set($key,$value,$expire = DAY_IN_SECONDS) {
		return set_transient($key,$value,$expire);
	}

	public static function get($key) {
		return get_transient($key);
	}

	public static function remove( $key ) {
		return delete_transient($key);
	}

}