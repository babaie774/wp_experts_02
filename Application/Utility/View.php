<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/18/2017
 * Time: 3:52 PM
 */

namespace Application\Utility;


class View {

	public static function load( string $path, array $params = null, string $layout = null ) { // admin.metaboxes.price.index
		$rawPath        = $path;
		$convertedPath  = str_replace( '.', DIRECTORY_SEPARATOR, $rawPath );
		$layoutFilePath = null;
		if ( ! is_null( $layout ) ) {
			$layoutPath     = str_replace( '.', DIRECTORY_SEPARATOR, $layout );
			$layoutFilePath = WPEXP_VIEWS . 'layouts/' . $layoutPath . '.php';
		}
		$viewPath = WPEXP_VIEWS . $convertedPath . '.php';
		if ( ! is_null( $layoutFilePath ) ) {
			if ( file_exists( $layoutFilePath ) && is_readable( $layoutFilePath ) ) {
				! is_null( $params ) ? extract( $params ) : null;
				include $layoutFilePath;

				return;
			}
		} else {
			if ( file_exists( $viewPath ) && is_readable( $viewPath ) ) {
				! is_null( $params ) ? extract( $params ) : null;
				include $viewPath;

				return;
			}
		}

		print "View File {$viewPath} Not Found.";

	}

	public static function render( string $path, array $params = null ) {
		$rawPath       = $path;
		$convertedPath = str_replace( '.', DIRECTORY_SEPARATOR, $rawPath );
		$viewPath      = WPEXP_VIEWS . $convertedPath . '.php';
		if ( file_exists( $viewPath ) && is_readable( $viewPath ) ) {
			! is_null( $params ) ? extract( $params ) : null;
			ob_start();
			include $viewPath;
			$content = ob_get_clean();

			return $content;
		}
		print "View File {$viewPath} Not Found.";
	}

}