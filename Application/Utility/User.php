<?php


namespace Application\Utility;


class User {

	public static function getWallet( int $user_id ) {
		return get_user_meta( $user_id, 'wallet', true );
	}

	public static function payFromWallet( int $user_id, int $amount ) {
		$currentWallet = self::getWallet( $user_id );
		if ( $currentWallet < $amount ) {
			return false;
		}
		$newWallet = $currentWallet - $amount;

		return update_user_meta( $user_id, 'wallet', $newWallet );
	}

}