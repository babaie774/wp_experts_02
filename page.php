<?php get_header(); ?>
<?php get_template_part( 'partials/top-bar' ); ?>
<?php get_template_part( 'partials/top-header' ); ?>
<?php get_template_part( 'partials/top-menu' ); ?>
	<div id="wrapper">
		<div id="content-wrap">
			<?php if(have_posts()): ?>
				<?php while (have_posts()):the_post(); ?>
					<div class="post">
						<h4 class="post-title">
							<?php the_title(); ?>
						</h4>
						<div class="post-excerpt">
							<?php the_content() ?>
						</div>
						<div class="post-details">
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<?php get_sidebar(); ?>
	</div>
    <?php// do_shortcode('asoifdagofasgogfasof [loginForm] oihaofidasgoufaso'); ?>
<?php get_footer() ?>