<!doctype html>
<html lang="fa_IR">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri().'/fonts.css'; ?>">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri().'/style.css'; ?>">
    <?php wp_head(); ?>
    <?php $wpx_options = wpx_options(); ?>
    <?php if(isset($wpx_options['color']['wpx-options-post-title-color'])): ?>
        <style>
            .post-title a{
                color:<?php echo $wpx_options['color']['wpx-options-post-title-color'];  ?>
            }
        </style>
    <?php endif; ?>
</head>
<body <?php body_class() ?>>