<?php /* Template Name: Login */ ?>
<?php
if ( \Application\Auth\Auth::check() ) {
	\Application\Utility\Url::redirect( '/' );
}

?>
<?php get_header(); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<div id="login-wrapper" class="animated">
    <form action="" method="post">
        <div class="form-row">
            ایمیل :
            <input type="email" id="user_email" name="email">
        </div>
        <div class="form-row">
            کلمه عبور :
            <input type="password" id="user_password" name="password">
        </div>
        <div class="form-row">
            <button type="submit" id="login_btn" name="submit_login">ورود</button>
        </div>
    </form>
</div>
