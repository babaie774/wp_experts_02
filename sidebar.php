<div id="sidebar-wrap">
<!--    --><?php //if(current_theme_supports('show-basket')): ?>
<!--    --><?php //show_basket(); ?>
<!--    --><?php //endif; ?>
	<?php
        $wpx_options = wpx_options();
        $showMainSidebar = true;

        if(isset($wpx_options['general']['wpx-options-main-sidebar-toggle']) && intval($wpx_options['general']['wpx-options-main-sidebar-toggle']) == 0)
        {
            $showMainSidebar = false;
        }

	  $showMainSidebar ? dynamic_sidebar('main-sidebar') : null;
    ?>
</div>