jQuery(document).ready(function ($) {
    var custom_uploader;
    $(document).on('click', '.wpx_select_slider_btn', function (e) {
        e.preventDefault();
        var $this = $(this);
        //If the uploader object has already been created, reopen the dialog
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }
        //Extend the wp.media object
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'انتخاب تصویر',
            button: {
                text: 'انتخاب تصویر'
            },
            multiple: false
        });
        //When a file is selected, grab the URL and set it as the text field's value
        custom_uploader.on('select', function () {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            console.log(attachment);
            $this.prev().attr('src',attachment.url);
            $this.next().val(attachment.url);
        });
        //Open the uploader dialog
        custom_uploader.open();
    });

});