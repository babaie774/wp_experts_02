jQuery(document).ready(function ($) {

    $(document).on('change', '#wpx_product_slider', function (event) {
       if($(this).attr('checked') == 'checked')
       {
            $('.wpx_slider_banner_wrapper').slideDown(300);
       }else
       {
           $('.wpx_slider_banner_wrapper').slideUp(300);
       }
    });

});