jQuery(document).ready(function ($) {
    $(document).on('click', '#login_btn', function (event) {
        event.preventDefault();
        var userEmail = $('#user_email').val();
        var userPassword = $('#user_password').val();
        var wrapper = $('#login-wrapper');
        $.ajax({
            url: wpx.ajaxUrl,
            type: 'post',
            dataType: 'json',
            data: {
                email: userEmail,
                password: userPassword,
                action: 'wpx_login'
            },
            success: function (response) {
                if (!response.success) {
                    swal({
                        title: 'خطا',
                        text: response.message,
                        icon: 'error'
                    });
                    //wrapper.addClass('hinge');
                } else {
                    swal({
                        title: 'لاگین موفق',
                        text: response.message,
                        icon: 'success'
                    });
                    window.location.href = response.redirect_to;
                }
            },
            error: function () {
            }
        });
    });
    $(document).on('click', '.add_to_basket_btn', function (event) {
        event.preventDefault();
        var product_id = $(this).data('pid');
        $('.basketCounterWrapper').removeClass('bounce');

        $.ajax({
            url: wpx.ajaxUrl,
            type: 'post',
            dataType: 'json',
            data: {
                product_id: product_id,
                action: 'wpx_add_to_basket'
            },
            success: function (response) {
                if (response.success) {
                    $('.basketCounter').text(response.basketItemsCount);
                    $('.basketCounterWrapper').addClass('bounce');

                }
            },
            error: function () {
            }
        });
    });
    // document.getElementById('#login_btn').addEventListener('click',function(){});
    $("#slider2").responsiveSlides({
        auto: true,
        pager: false,
        speed: 300,
        maxwidth: 1000
    });

    $(document).on('click','.order_address .item',function (event) {
        var $this = $(this);
        $('.itemContent').slideUp(200);
        $this.next().slideDown(300);
    });
});