<?php
class Autoloader {

	public static function autoload( $class ) {
		$classParts = explode( '\\', $class );
		$className  = implode( '/', $classParts );
		$className  = WPEXP_DIR.DIRECTORY_SEPARATOR.$className.'.php';
		if ( file_exists( $className ) ) {
			include_once $className;
		}
	}
	
}