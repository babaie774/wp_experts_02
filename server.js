const server = require('http').createServer();
const io = require('socket.io')(server);

io.on('connection',function(socket){

    console.log('a user connected.');
    socket.on('sendContent',function (data) {
        console.log(data);
        socket.broadcast.emit('publishMessage',data);
    });
});

server.listen(3000);