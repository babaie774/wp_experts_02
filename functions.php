<?php

//global $wp_rewrite;
//
//var_dump($wp_rewrite);

include "constants.php";
include "helpers/helpers.php";
include "Autoloader.php";
spl_autoload_register( 'Autoloader::autoload' );

add_action( 'after_setup_theme', 'Application\Init::theme_setup' );
add_action( 'init', 'Application\Init::start_session', 1 );
add_action( 'init', 'Application\Init::check_wishlist' );
add_filter( 'show_admin_bar', '__return_false' );
add_action( 'wp_enqueue_scripts', 'Application\Asset::load' );
add_action( 'admin_enqueue_scripts', 'Application\Asset::load_admin' );
//add_action('wp_ajax_wpx_login','Application\Ajax::login');//with login
add_action( 'wp_ajax_nopriv_wpx_login', 'Application\Ajax::login' );
add_action( 'wp_ajax_wpx_add_to_basket', 'Application\Ajax::addToBasket' );
add_action( 'wp_ajax_nopriv_wpx_add_to_basket', 'Application\Ajax::addToBasket' );
add_action( 'init', 'Application\Content\PostType\WpxPostTypes::init' );
add_action( 'init', 'Application\Content\Taxonomies\WpxTaxonomies::register' );
\Application\Admin\Metaboxes\MetaBoxes::load();
add_action( 'parse_request', '\Application\Service\Router\Router::register' );
function member_content( $atts, $content = null ) {
	if ( \Application\Auth\Auth::check() ) {
		return $content;
	}

	return '<div class="only_members_contents"><p>این محتوا مخصوص کاربران عضو شده در وب سایت می باشد</p></div>';
}

add_shortcode( 'member', 'member_content' );
function download_link_generator( $atts ) {

	$args = shortcode_atts( [
		'class' => 'download_link',
		'text'  => 'دانلود'
	], $atts, 'downloadLink' );

	$download_link  = $atts['link'];
	$download_class = $args['class'];
	$download_text  = $args['text'];

	return '<div class="' . $download_class . '"><a href="' . $download_link . '">' . $download_text . '</a></div>';
}

add_shortcode( 'downloadLink', 'download_link_generator' );
add_shortcode( 'wishlist', '\Application\Content\Shortcode\Wishlist::callback' );
new \Application\Content\FormShortCode();
\Application\Content\Widgets\WidgetsLoader::register();
function wpx_add_plugin( $plugins ) {
	$plugins['members'] = WPEXP_JS_URL . 'plugins/members.js';

	return $plugins;
}

function wpx_register_buttons( $buttons ) {
	array_push( $buttons, 'members' );

	return $buttons;
}

add_filter( 'mce_external_plugins', 'wpx_add_plugin' );
add_filter( 'mce_buttons', 'wpx_register_buttons' );
include "options-panel/panel.php";
require_once get_template_directory() . '/cs-framework/cs-framework.php';

add_action( 'admin_menu', 'register_admin_page_for_layouts' );

function register_admin_page_for_layouts() {

	$users_page_hook = add_menu_page(
		'مدیریت کاربران',
		'مدیریت کاربران',
		'manage_options',
		'wpx_users',
		'wpx_users_management'
	);
	add_action( "load-$users_page_hook", function () {
		add_action( 'admin_enqueue_scripts', function () {
			wp_enqueue_script( 'chart-js', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.min.js' );
		} );
	} );
}

function wpx_users_management() {

	$users = [
		'دی'    => 243,
		'بهمن'  => 159,
		'اسفند' => 563
	];
	$sales = [
		'دی'    => 1253,
		'بهمن'  => 498,
		'اسفند' => 2320
	];
	\Application\Utility\View::load( 'admin.users.index', [
		'users' => $users,
		'sales' => $sales,
	], 'admin.users' );

}

require_once get_template_directory().'/tgm/class-tgm-plugin-activation.php';

add_action('tgmpa_register',function (){
	$plugins = array(

		// This is an example of how to include a plugin bundled with a theme.
		array(
			'name'               => 'TGM Example Plugin', // The plugin name.
			'slug'               => 'tgm-example-plugin', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),

		// This is an example of how to include a plugin from an arbitrary external source in your theme.
		array(
			'name'         => 'TGM New Media Plugin', // The plugin name.
			'slug'         => 'tgm-new-media-plugin', // The plugin slug (typically the folder name).
			'source'       => 'https://s3.amazonaws.com/tgm/tgm-new-media-plugin.zip', // The plugin source.
			'required'     => true, // If false, the plugin is only 'recommended' instead of required.
			'external_url' => 'https://github.com/thomasgriffin/New-Media-Image-Uploader', // If set, overrides default API URL and points to an external URL.
		),

		// This is an example of how to include a plugin from a GitHub repository in your theme.
		// This presumes that the plugin code is based in the root of the GitHub repository
		// and not in a subdirectory ('/src') of the repository.
		array(
			'name'      => 'Adminbar Link Comments to Pending',
			'slug'      => 'adminbar-link-comments-to-pending',
			'source'    => 'https://github.com/jrfnl/WP-adminbar-comments-to-pending/archive/master.zip',
		),

		// This is an example of how to include a plugin from the WordPress Plugin Repository.
		array(
			'name'      => 'BuddyPress',
			'slug'      => 'buddypress',
			'required'  => false,
		),

		// This is an example of the use of 'is_callable' functionality. A user could - for instance -
		// have WPSEO installed *or* WPSEO Premium. The slug would in that last case be different, i.e.
		// 'wordpress-seo-premium'.
		// By setting 'is_callable' to either a function from that plugin or a class method
		// `array( 'class', 'method' )` similar to how you hook in to actions and filters, TGMPA can still
		// recognize the plugin as being installed.
		array(
			'name'        => 'WordPress SEO by Yoast',
			'slug'        => 'wordpress-seo',
			'is_callable' => 'wpseo_init',
		),

	);
	$config = array(
		'id'           => 'wpx',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.

		/*
		'strings'      => array(
			'page_title'                      => __( 'Install Required Plugins', 'wpx' ),
			'menu_title'                      => __( 'Install Plugins', 'wpx' ),
			/* translators: %s: plugin name. * /
			'installing'                      => __( 'Installing Plugin: %s', 'wpx' ),
			/* translators: %s: plugin name. * /
			'updating'                        => __( 'Updating Plugin: %s', 'wpx' ),
			'oops'                            => __( 'Something went wrong with the plugin API.', 'wpx' ),
			'notice_can_install_required'     => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'wpx'
			),
			'notice_can_install_recommended'  => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'wpx'
			),
			'notice_ask_to_update'            => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'wpx'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
				/* translators: 1: plugin name(s). * /
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'wpx'
			),
			'notice_can_activate_required'    => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'wpx'
			),
			'notice_can_activate_recommended' => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'wpx'
			),
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'wpx'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'wpx'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'wpx'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'wpx' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'wpx' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'wpx' ),
			/* translators: 1: plugin name. * /
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'wpx' ),
			/* translators: 1: plugin name. * /
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'wpx' ),
			/* translators: 1: dashboard link. * /
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'wpx' ),
			'dismiss'                         => __( 'Dismiss this notice', 'wpx' ),
			'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'wpx' ),
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'wpx' ),

			'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),
		*/
	);
	tgmpa( $plugins, $config );
});

add_action( 'wp_dashboard_setup', 'wpx_api_data_widget' );

function wpx_api_data_widget(){
	wp_add_dashboard_widget(
		'api_data_fetcher',         // Widget slug.
		'آخرین مطالب وب سایت ',         // Title.
		'api_data_fetcher_callback' // Display function.
	);
}

function api_data_fetcher_callback(){
	$data = \Application\Utility\WPX_Cache::get('api_data');
	$posts = [];
	if($data === false)
	{
		$response = wp_remote_get('https://hamyarwp.com/wp-json/wp/v2/posts');
		if(is_array($response))
		{
			$content = $response['body'];
			$data = json_decode($content);
			\Application\Utility\WPX_Cache::set('api_data',$data);

		}
	}
	foreach ($data as $item)
	{
		$posts[] = [
			'title' => $item->title->rendered,
			'link'  => $item->link
		];
	}



	?>
	<?php foreach ($posts as $post): ?>
	<p>
		<a target="_blank" href="<?php echo $post['link']; ?>"><?php echo $post['title']; ?></a>
	</p>
	<?php endforeach; ?>
	<?php

}

add_action('parse_request',function($query){

    if(strpos($_SERVER['REQUEST_URI'],'wp-json') !== false)
    {
        wp_send_json([
                'status' => false,
                'message' => 'access denied.'
        ]);
        exit;
    }
},1);

add_action('init',function($query){

	if(strpos($_SERVER['REQUEST_URI'],'wp-admin') !== false)
	{
        if(!current_user_can('manage_options'))
        {
            print 'access denied.';
	        exit;
        }

	}
},2);


