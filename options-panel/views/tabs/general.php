<div class="wpx-options-tab-general">
    <div class="option-row">
        <label class="ios7-switch">
            <input
                    type="checkbox"
                    name="wpx-options-main-sidebar-toggle"
                    id="wpx-options-main-sidebar-toggle"
                    <?php checked(1,isset( $wpx_options['general']['wpx-options-main-sidebar-toggle']) ? intval($wpx_options['general']['wpx-options-main-sidebar-toggle']) : 0) ?>
                    >
            <span></span>
            نمایش ساید بار اصلی سایت
        </label>
    </div>
    <div class="options-row">
        <?php //wp_nonce_field('save_general_settings','save_general_nonce'); ?>
        <button  type="submit" class="button-primary" name="save_settings">ذخیره تنظیمات</button>
    </div>
</div>