<?php

abstract class WPX_Section_Contract {

	public static $title;

	public static $tab;

	public $viewFile;

	protected $wpx_options;

	public function __construct() {
		$this->wpx_options = wpx_options();
	}

	abstract public function render();

	abstract public function save();

	protected function view(){

	}

}