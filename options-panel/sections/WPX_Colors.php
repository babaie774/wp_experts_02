<?php
class WPX_Color extends WPX_Section_Contract {
	public static $title = 'رنگ بندی';

	public static $tab = 'color';

	public function __construct() {
		parent::__construct();
		$this->viewFile = PANEL_VIEWS . 'tabs/color.php';
	}

	public function render() {
		$wpx_options= $this->wpx_options;
		include $this->viewFile;
	}

	public function save() {
		$this->wpx_options['color']['wpx-options-post-title-color'] = ! empty( $_POST['wpx-options-post-title-color'] ) ? $_POST['wpx-options-post-title-color'] : '';
		wpx_save_options($this->wpx_options);
	}
}