<?php


class WPX_General extends WPX_Section_Contract {

	public static $title = 'عمومی';
	public static $tab = 'general';

	public function __construct() {
		parent::__construct();
		$this->viewFile = PANEL_VIEWS . 'tabs/general.php';
	}

	public function render() {
//		if ( ! Application\Service\Acl\AclService::currentCan( 'manage_instagram' ) ) {
//			wp_die( 'error' );
//		}
		$wpx_options = $this->wpx_options;
		include $this->viewFile;
	}

	public function save() {
		if(!wp_verify_nonce($_REQUEST['save_general_nonce'],'save_general_settings'))
		{
			die('شما مجوز دسترسی برای انجام این عملیات را ندارید.');
		}

		$this->wpx_options['general']['wpx-options-main-sidebar-toggle'] = isset( $_POST['wpx-options-main-sidebar-toggle'] ) ? 1 : 0;
		wpx_save_options( $this->wpx_options );

	}
}